package com.example;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Random;

import com.example.components.ClientUtility;
import com.example.model.MarketUpdate;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

/**
 * This class creates a Hazelcast cluster with clusterSize nodes hosted in the same JVM.
 * It starts a background thread
 * @author nicholas
 *
 */
public class MarketManager {
	
	//static 
	private static MarketManager clusterUtility;
	
	public static String mapName = "markets-map";
	final static Random random = new Random();
	LinkedHashMap<String, MarketUpdate> marketMap = new LinkedHashMap<String, MarketUpdate>(); 

	static Double generateInitialPrice(){
		return 10.0d + (double)random.nextInt(10);
	}
	
//	static {		
//		marketMap.put("Singapore", new MarketUpdate("Singapore", generateInitialPrice()));
//		marketMap.put("Shanghai", new MarketUpdate("Shanghai", generateInitialPrice()));
//		marketMap.put("Amsterdam", new MarketUpdate("Amsterdam", generateInitialPrice()));
//		marketMap.put("Paris", new MarketUpdate("Paris", generateInitialPrice()));
//		marketMap.put("Tokyo", new MarketUpdate("Tokyo", generateInitialPrice()));
//		marketMap.put("London", new MarketUpdate("London", generateInitialPrice()));		
//	}

	public LinkedHashMap<String, MarketUpdate> getMarketMap() {
		return marketMap;
	}

	public void setMarketMap(LinkedHashMap<String, MarketUpdate> marketMap) {
		this.marketMap = marketMap;
	}

	public MarketManager(){
		//a simple pre-load step which we'll use to populate the datagrid map later
		marketMap.put("Singapore", new MarketUpdate("Singapore", generateInitialPrice()));
		marketMap.put("Shanghai", new MarketUpdate("Shanghai", generateInitialPrice()));
		marketMap.put("Amsterdam", new MarketUpdate("Amsterdam", generateInitialPrice()));
		marketMap.put("Paris", new MarketUpdate("Paris", generateInitialPrice()));
		marketMap.put("Tokyo", new MarketUpdate("Tokyo", generateInitialPrice()));
		marketMap.put("London", new MarketUpdate("London", generateInitialPrice()));
	}
	
	public class MarketUpdater implements Runnable {
		
		private Random random = new Random();
		
		//This thread updates the map in the datagrid, this is our reference to it
		private IMap<String, MarketUpdate> gridMarketMap;

		private Double maximumMovement = 10.5D;
		
		private boolean stopFlag = false;
		
		/**
		 * Allow the creation of a client instance to our map name
		 * @param mapName
		 */
		MarketUpdater() {
			HazelcastInstance client = ClientUtility.createClient();
			gridMarketMap = client.getMap(mapName);
		}
		
		private double roundPrice(boolean up, double value, double delta){
			if(up)
				value += delta;
			else
				value -= delta;
			return BigDecimal.valueOf(value).setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue();
		}
		
		@Override
		public void run() {

			while (!stopFlag) {

				int marketIdToUpdate = random.nextInt(gridMarketMap.size());
				MarketUpdate marketToUpdate = gridMarketMap.get(gridMarketMap.keySet().toArray()[marketIdToUpdate]);
					
				Double currentValue = marketToUpdate.getValue();
				Double change = random.nextDouble() * maximumMovement;
				boolean marketUp = random.nextBoolean();
				
				//ensure we increase if we would get a negative value on this update
				if (!marketUp && (currentValue - change <0)){
					marketToUpdate.setValue(roundPrice(!marketUp, currentValue, change));
				}else {
					marketToUpdate.setValue(roundPrice(marketUp, currentValue, change));
				}						
				
				marketToUpdate.setLastUpdate(new Date());

				gridMarketMap.put(marketToUpdate.getName(), marketToUpdate);
				
				System.out.println("Updated market value for " + marketToUpdate.getName() + " : " + marketToUpdate.getValue());
				
				try {
					Thread.sleep(1500L);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {} 
			}
			
			System.out.println("Update thread flag stopped, shutting down.");
			
		}

		public void setStopFlag(boolean stop) {
			this.stopFlag = stop;
		}
	}
	
}

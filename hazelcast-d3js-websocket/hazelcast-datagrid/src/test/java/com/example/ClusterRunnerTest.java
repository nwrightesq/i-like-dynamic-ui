package com.example;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import com.example.components.ClientUtility;
import com.example.components.ClusterUtility;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;



//import test.utilities.ClusterUtility;
import com.example.components.ClientUtility;
import com.example.model.MarketUpdate;
import com.hazelcast.core.Cluster;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

/**
 * This class creates an in JVM HazelCast Cluster.
 * It uses the MarketManager to spin up a background thread and update the cluster.
 * @author nicholas
 *
 */
public class ClusterRunnerTest {
	
	static MarketManager marketManager;

	private static int initialClusterSize = 2;	
	private static int dataBackupCount = 1;
	
	private static ClusterUtility clusterUtility ;
	
	public static void populateStockDataset(String mapName, LinkedHashMap<String, MarketUpdate> markets, int backupCount) {
        clusterUtility.buildAndApplyMapConfig(mapName, backupCount);
        
        Map<String, MarketUpdate> map = clusterUtility.getRandomInstance().getMap(mapName);
        
        for (Map.Entry<String, MarketUpdate> marketEntry : markets.entrySet()) {
        	System.out.println("inserting " + marketEntry.getKey() + " value : " + marketEntry.getValue());
        	map.put(marketEntry.getKey(), marketEntry.getValue());
        }
	}

	@BeforeClass
	public static void setupCluster() {
		clusterUtility = new ClusterUtility(initialClusterSize);
		marketManager = new MarketManager();
		
		//pupulate the datagrid with our marketManager initial dataset		
		populateStockDataset(MarketManager.mapName, marketManager.getMarketMap(), dataBackupCount);
	}

	@AfterClass
	public static void teardownCluster() {
		clusterUtility.shutdown();
	}

	@Test
	public void startClusterAndUpdater() throws InterruptedException, ExecutionException, IOException {

		System.out.println("In Test");
		HazelcastInstance client = ClientUtility.createClient();
		
		//MarketManager manager = new MarketManager();
		MarketManager.MarketUpdater marketManagerUpdater = marketManager.new MarketUpdater();
		Thread updaterThread = new Thread(marketManagerUpdater); 

		updaterThread.start();
        
        Cluster cluster = client.getCluster();
        assertTrue(cluster.getMembers().size() == initialClusterSize);

        //wait for an update
        System.in.read();
        
        marketManagerUpdater.setStopFlag(true);
        
        client.shutdown();

	}
}

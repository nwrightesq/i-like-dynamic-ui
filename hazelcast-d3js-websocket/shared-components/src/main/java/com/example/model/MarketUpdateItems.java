package com.example.model;

import java.util.Collection;

/**
 * Simple container class that we can serialise to JSON. 
 * Use this to hold a list of key names to stream to a UI when it first connects.
 * @author nicholas
 *
 */
public class MarketUpdateItems {

	private Collection <String> markets;

	public Collection<String> getMarkets() {
		return markets;
	}

	public void setMarkets(Collection<String> markets) {
		this.markets = markets;
	}
	
	
}

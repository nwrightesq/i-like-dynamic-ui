package com.example.model;

import java.io.Serializable;
import java.util.Date;

public class MarketUpdate implements Serializable{

	/**
	 * No args constructor for Serialisation
	 */
	public MarketUpdate(){}
	
	public MarketUpdate(String name, double value) {
		super();
		this.name = name;
		this.value = value;
		this.lastUpdate = new Date();
	}

	private String name;
	private double value;
	private Date lastUpdate;
	
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "MarketUpdate [name=" + name + ", value=" + value
				+ ", lastUpdate=" + lastUpdate + "]";
	}
		
}

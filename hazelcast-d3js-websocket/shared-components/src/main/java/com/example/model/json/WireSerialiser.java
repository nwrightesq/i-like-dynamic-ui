package com.example.model.json;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.map.ObjectMapper;

public class WireSerialiser {

	private final ObjectMapper mapper;

	private WireSerialiser() {
		JsonFactory factory = new JsonFactory();
		mapper = new ObjectMapper(factory);
	}

	private static class WireSerialiserHolder {
		private static final WireSerialiser INSTANCE = new WireSerialiser();
	}

	public static WireSerialiser getInstance() {
		return WireSerialiserHolder.INSTANCE;
	}
	
	public String serialize(Object response) {
		try {
			String responseString = mapper.writeValueAsString(response);
			System.out.println("JSON Wire Format: " + responseString);
			return responseString;
		} catch (Exception e) {
			System.out.println("Unable to serialize Object: [" + response + "]");
			return null;
		}
	}
	
}

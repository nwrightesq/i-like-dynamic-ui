package com.example.components;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.LifecycleEvent;
import com.hazelcast.core.LifecycleListener;
import com.hazelcast.core.LifecycleService;
import com.hazelcast.core.MembershipEvent;
import com.hazelcast.core.MembershipListener;

/**
 * This Factory can be used to create connections to a datagrid cluster 
 * @author nicholas
 */
public class ClientUtility {

	/**
	 * Factory wrapper for creating clients to the Hazelcast Cluster
	 * @return return an instance. 
	 */
	public static HazelcastInstance createClient() {
		return create(true, true);
	}

	/**
	 * Create a client with optional lifecycle listeners.
	 * @param addLifecycleListener
	 * @param addMembershipListener
	 * @return
	 */
	public static HazelcastInstance create(boolean addLifecycleListener, boolean addMembershipListener) {

		//create a default config
		ClientConfig clientConfig = new ClientConfig();
		
		//connect to the default hazelcast listen port
		clientConfig.addAddress("127.0.0.1:5701");
		
		// Use the stock Hazelcast client mechanism.
		HazelcastInstance client = HazelcastClient.newHazelcastClient(clientConfig);
 
		//configure as needed
		if (addLifecycleListener) {
			addLifecycleListener(client);
		}

		if (addMembershipListener) {
			addMembershipListener(client);
		}

		return client;

	}

	/**
	 * Tell us about clients leaving and joining the cluster
	 * @param client
	 */
	private static void addLifecycleListener(HazelcastInstance client) {

		//use the service...
		LifecycleService lifecycleService = client.getLifecycleService();
		
		//to add a listener.
		lifecycleService.addLifecycleListener(new LifecycleListener() {
			
			//and tell us when events happen
			public void stateChanged(LifecycleEvent event) {
				if (event.getState() == LifecycleEvent.LifecycleState.CLIENT_DISCONNECTED) {
					System.out.println("CLIENT -> Disconnected");
				} else if (event.getState() == LifecycleEvent.LifecycleState.CLIENT_CONNECTED) {
					System.out.println("CLIENT -> Connected");
				} else {
					System.out.println(event.getState());
				}
			}
		});

	}

	/**
	 * Tell us about cluster nodes joining and leaving the cluster
	 * @param client
	 */
	private static void addMembershipListener(HazelcastInstance client) {

		//create a membership listener
		client.getCluster().addMembershipListener(new MembershipListener() {
			
			//to show nodes leaving and joining
			public void memberRemoved(MembershipEvent membershipEvent) {
				System.out.println("CLUSTER MEMBER -> removed : " + membershipEvent.getMember().getInetSocketAddress());
			}

			public void memberAdded(MembershipEvent membershipEvent) {
				System.out.println("CLUSTER MEMBER -> added : " + membershipEvent.getMember().getInetSocketAddress());
			}
		});

	}
}

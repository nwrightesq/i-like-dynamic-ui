package com.example.components;
import java.util.LinkedList;
import java.util.Random;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

public class ClusterUtility {
	
	private static final Random random = new Random();
	
	//a list to hold references to our cluster instances. used to shutdown, configure, etc
	private final LinkedList<HazelcastInstance> instances = new LinkedList<HazelcastInstance>();
	
	private final Config config;
	
	public ClusterUtility(int clusterSize) {
        config = new Config();
		for (int i = 0 ; i < clusterSize ; i++) {
			addClusterMember();
		}
	}

	public void shutdown() {
		for (HazelcastInstance instance : instances) {
			shutdownInstance(instance);
		}
	}

	public void shutdownRandomInstance() {
		shutdownInstance(getRandomInstance());
	}

	public void shutdownInstance(int instanceToShutdown) {
		shutdownInstance(instances.get(instanceToShutdown));
	}

	private void shutdownInstance(HazelcastInstance instance) {
		instance.shutdown();
	}
	
	public HazelcastInstance getRandomInstance() {
		int randomInstanceIndex = random.nextInt(instances.size());
		return instances.get(randomInstanceIndex);
	}

	public void buildAndApplyMapConfig(String mapName, int backupCount) {
        MapConfig mapConfig = new MapConfig();
        mapConfig.setName(mapName);
        mapConfig.setBackupCount(backupCount);
        for (HazelcastInstance instance : instances) {
        	instance.getConfig().addMapConfig(mapConfig);
        }
	}
	
	public void addClusterMember() {
        HazelcastInstance instance = Hazelcast.newHazelcastInstance(config);
        instances.add(instance);
	}

	public void removeLastClusterMember() {
		shutdownInstance(instances.pollLast());
	}
	public void removeFirstClusterMember() {
		shutdownInstance(instances.pollFirst());
	}
}
